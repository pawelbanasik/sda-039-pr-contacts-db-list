package com.example.amen.internaldatabasecontactsapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.example.amen.internaldatabasecontactsapplication.controler.ContactsDatabaseHelper;
import com.example.amen.internaldatabasecontactsapplication.model.Contact;

public class MainActivity extends AppCompatActivity {


    private ListView list;
    private ArrayAdapter<Contact> array;
    private ContactsDatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new ContactsDatabaseHelper(getApplicationContext());

        list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MainActivity.this, ContactEditActivity.class);
                i.putExtra("contact", array.getItem(position));

                startActivity(i);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                PopupWindow window = createPopup(position);
                window.showAsDropDown(view, 0, 0);


                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_add_contact) {
            Intent i = new Intent(this, ContactEditActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateList() {
        array = new ArrayAdapter<Contact>(this, android.R.layout.simple_list_item_1, dbHelper.readContacts());
        list.setAdapter(array);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateList();
    }

    public PopupWindow createPopup(final int position) {
        final PopupWindow window = new PopupWindow(this);

        Button newButton = new Button(this);
        newButton.setText("Delete");
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.deleteContact(array.getItem(position));
                array.remove(array.getItem(position));
                window.dismiss();
            }
        });
        window.setFocusable(true);
        window.setContentView(newButton);
        window.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

        return window;
    }
}
