package com.example.amen.internaldatabasecontactsapplication.controler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.example.amen.internaldatabasecontactsapplication.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amen on 1/21/17.
 */

public class ContactsDatabaseHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;

    //
    private static final String TABLE_NAME = "contacts";
    private static final String DATABASE_NAME = "contacts.db";

    //
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_FIRST_NAME = "first_name";
    private static final String COLUMN_SURNAME = "last_surname";
    private static final String COLUMN_PHONE = "phone_number";

    public ContactsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO: stworzenie bazy
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ( " +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_FIRST_NAME + " VARCHAR, " +
                COLUMN_SURNAME + " VARCHAR, " +
                COLUMN_PHONE + " VARCHAR); "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void createContact(Contact contact) {              // create from CRUD
        SQLiteDatabase db = getWritableDatabase();

        // TODO: insert - wstawianie

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_ID + ", " +
                COLUMN_FIRST_NAME + ", " +
                COLUMN_SURNAME + ", " +
                COLUMN_PHONE +
                ") "
                + " VALUES ( NULL, '"
                + contact.getFirstName()
                + "', '"
                + contact.getSurname()
                + "', '"
                + contact.getPhoneNumber() + "')");

        db.close();
    }

    public List<Contact> readContacts() {                // read
        List<Contact> list = new ArrayList<>();

        // TODO: załadowanie
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

        // iterujemy kursor, przechadzamy się między wierszami, więc na początek
        // trzeba przejść do pierwszego (move to next)
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToNext();

            // pobieram 3 wartosci
            int id = cursor.getInt(0);
            String firstName = cursor.getString(1);
            String surName = cursor.getString(2);
            String phone = cursor.getString(3);

            // tworze obiekt, ustawiam 3 wartości
            Contact newContact = new Contact();
            newContact.setId(id);
            newContact.setFirstName(firstName);
            newContact.setSurname(surName);
            newContact.setPhoneNumber(phone);

            // dodaje do listy
            list.add(newContact);


        }
        return list;
    }


    public void updateContact(Contact contact) {              // update
        SQLiteDatabase db = getWritableDatabase();

        // TODO: update

        ContentValues cv = new ContentValues();

        cv.put(COLUMN_FIRST_NAME, contact.getFirstName());
        cv.put(COLUMN_SURNAME, contact.getSurname());
        cv.put(COLUMN_PHONE, contact.getPhoneNumber());

        db.update(TABLE_NAME, cv, COLUMN_ID +"=?", new String[]{String.valueOf(contact.getId())});

        db.close();
    }

    public void deleteContact(Contact contact) {              // delete
        SQLiteDatabase db = getWritableDatabase();

        // TODO: usuniecie
        db.delete(TABLE_NAME, COLUMN_ID +"=?", new String[]{String.valueOf(contact.getId())});


        db.close();
    }

}
